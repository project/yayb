<?php

namespace Drupal\yayb\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\yayb\YaybService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines the YAYB REST API controller.
 */
class YaybApiController implements ContainerInjectionInterface {

  /**
   * Stores the YAYB service.
   *
   * @var \Drupal\yayb\YaybService
   */
  protected $yayb;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('yayb')
    );
  }

  /**
   * Constructs a new Controller object.
   *
   * @param \Drupal\yayb\YaybService $yayb
   *   The YAYB service.
   */
  public function __construct(YaybService $yayb) {
    $this->yayb = $yayb;
  }

  /**
   * Return the API contents.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A Symfony JSON Response.
   */
  public function view(): JsonResponse {
    $payload = $this->yayb->view() ?? [];
    return new JsonResponse($payload);
  }

}
