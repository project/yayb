<?php

namespace Drupal\yayb\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\yayb\YaybService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Yellow Box" alert block.
 *
 * @Block(
 *   id = "yayb_block",
 *   admin_label = @Translation("Yet Another Yellow Box"),
 * )
 */
class YaybBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Stores the YAYB service.
   *
   * @var \Drupal\yayb\YaybService
   */
  protected $yayb;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('yayb')
    );
  }

  /**
   * Constructs a new BANE block object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\yayb\YaybService $yayb
   *   The YAYB service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    YaybService $yayb,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->yayb = $yayb;
  }

  /**
   * Displays the block contents.
   */
  public function build() {

    if ($alert = $this->yayb->view()) {
      return $alert;
    }
    return NULL;
  }

}
