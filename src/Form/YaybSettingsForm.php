<?php

namespace Drupal\yayb\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the Notification Settings Form.
 */
class YaybSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'yayb.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yayb_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);

    $datetime = $config->get('yayb_expiration') ?? NULL;
    if (NULL !== $datetime) {
      $datetime = new DrupalDateTime($datetime);
    }

    $form[] = [
      '#markup' => $this->t('<div><strong>Note: Page and block caching may affect visibility.</strong></div>'),
    ];
    $form['yayb_toggle'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display Options'),
      '#default_value' => $config->get('yayb_toggle') ?? 0,
      '#options' => [
        1 => $this->t('Show Alert Box'),
        0 => $this->t('Hide Alert Box'),
      ],
      '#description' => $this->t('Current message visibility. Must be on even when using expiration time below.'),
    ];
    $form['yayb_expiration'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Expiration Date'),
      '#default_value' => $datetime,
      '#description' => $this->t('If this field is blank, the message will be displayed until disabled manually.'),
    ];
    $form['yayb_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('yayb_title') ?? $this->t('Important Message'),
      '#description' => $this->t('Title for message block.'),
    ];
    $form['yayb_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value' => $config->get('yayb_message') ?? NULL,
      '#description' => $this->t('Message to display to users. This will be passed through the default input filter.'),
    ];
    $form['yayb_severity'] = [
      '#type' => 'radios',
      '#title' => $this->t('Severity'),
      '#options' => [
        'critical' => $this->t('<span class="yayb-critical">Critical</span>'),
        'major' => $this->t('<span class="yayb-major">Major</span>'),
        'important' => $this->t('<span class="yayb-important">Important</span>'),
        'notice' => $this->t('<span class="yayb-notice">Notice</span>'),
        'info' => $this->t('<span class="yayb-info">Info</span>'),
      ],
      '#default_value' => $config->get('yayb_severity') ?? 'important',
      '#description' => $this->t('Set the severity level of the message. (Use css styling in theme to alter the display.)'),
    ];

    $form['#attached']['library'][] = 'yayb/yayb-styling';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('yayb_toggle', $form_state->getValue('yayb_toggle'))
      ->set('yayb_expiration', $form_state->getValue('yayb_expiration')->format('Y-m-d H:i:s'))
      ->set('yayb_title', $form_state->getValue('yayb_title'))
      ->set('yayb_message', $form_state->getValue('yayb_message'))
      ->set('yayb_severity', $form_state->getValue('yayb_severity'))
      ->save();

    parent::submitForm($form, $form_state);

    // Clear caches to retstore block visibility.
    drupal_flush_all_caches();
  }

}
