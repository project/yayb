<?php

namespace Drupal\yayb;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Defines the YAYB alert service.
 *
 * @todo Make an interface for this!
 */
class YaybService {

  /**
   * Stores the module configuration info.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TimeInterface $time) {
    $this->config = $config_factory->get('yayb.settings');
    $this->time = $time;
  }

  /**
   * Displays the block contents.
   */
  public function view(): ?array {
    if ($this->checkVisibility()) {
      $block = [];
      if ($subject = $this->config->get('yayb_title')) {
        $block['subject'] = ['#plain_text' => $subject];
      }
      if ($content = $this->config->get('yayb_message')) {
        $block['content'] = ['#markup' => $content];
      }
      $block['severity'] = $this->config->get('yayb_severity');
      return $block;
    }
    return NULL;
  }

  /**
   * Check block visibility.
   *
   * @return bool
   *   Returns TRUE if the block should be shown.
   */
  public function checkVisibility(): bool {
    if (!$this->config->get('yayb_toggle')) {
      return FALSE;
    }
    if (empty($this->config->get('yayb_expiration'))) {
      return TRUE;
    }
    return strtotime($this->config->get('yayb_expiration')) > $this->time->getRequestTime();
  }

}
