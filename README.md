Yet Another Yellow Box
======================

This module allows for the creation of a block that allows trusted users to
post alert messages to on the website. Block administrators may edit the
content of the message (filtered by the site's default input filter) and may
toggle display of the block on or off or set an expiration date for the block
to disappear automatically.

Block information is also exposed using a REST endpoint, allowing the message
to be displayed on other websites.

Install like any other Drupal module, then place the block in the desired
region. If desired, limit where block is displayed - usually the home page -
and grant appropriate permissions to the role who should administer the
message.
